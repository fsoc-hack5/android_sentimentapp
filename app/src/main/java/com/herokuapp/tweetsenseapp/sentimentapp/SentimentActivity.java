package com.herokuapp.tweetsenseapp.sentimentapp;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SentimentActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    JSONObject data = null;
    private String pos, neg, neutral;
    private static String TAG = "SentimentActivity";
    private float[] yData = new float[3];
    private String[] xData = {"Positive","Negative","Neutral"};
    PieChart pieChart;
    TextView sentimentScoreTitle;
    TextView sentimentScoreField;

    Handler h = new Handler();
    int delay = 13*1000; //1 second=1000 milisecond, 13*1000=13seconds
    Runnable runnable;
    private String accessToken = "3ca63768cbdca6fd6e3ffbc5bfe3a6eca94b2ed1";

    ProgressBar mProgressBar;

    LineChart lineChart;
    static long timeValue=0;

    //Arraylist for line chart
    ArrayList<Entry> yValues = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentiment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Log.d(TAG, "created!");
//        getSentiment();
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getSentiment();
//                handler.postDelayed(this, 5000);
//            }
//        }, 5000);
//        getSentiment();

        Log.d("Sentiment activity:", "completed!");

        //assigning and staring the progress bar
        mProgressBar=(ProgressBar)findViewById(R.id.mProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        //Assinging id
        sentimentScoreField = (TextView)findViewById(R.id.txtSentimentScore);
        sentimentScoreTitle = (TextView)findViewById(R.id.sentimentScoreTitle);

        Log.d(TAG,"onCreate: starting to create chart");

        /** Code for PieChart - Start*/
        pieChart=(PieChart)findViewById(R.id.pieChart);
        pieChart.setContentDescription("Tweets' sentiments");
        pieChart.getDescription().setText("Tweets' sentiments");
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Sentiment Chart");
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);

        //To animate piechart as per the time selected in spinner
        Intent i=getIntent();
//        String pieChartTime=i.getStringExtra("time");
        String pieChartTime = "10";  // 10 sec as server runs for 10 sec for on API call
        int time=Integer.parseInt(pieChartTime); //here time is in seconds
        Log.d("Spinner Time",pieChartTime);
        //converting time to miliseconds
        pieChart.animateXY((time*1000+5000),(time*1000+5000));

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.d(TAG, "onValueSelected: Value select from chart");
                Log.d(TAG,"onValueSelected "+ e.toString());
                Log.d(TAG,"onValueSelected "+ h.toString());

                int pos1 = e.toString().indexOf("y: "); // total characters in "y: " = 3(Three)
                String tweets= e.toString().substring(pos1 + 3);

                Log.d(TAG,"Sentiment Score: "+tweets);

                for (int i=0;i<yData.length;i++){
                    if(yData[i]==Float.parseFloat(tweets)){
                        pos1=i;
                        break;
                    }
                }
                String Opinion =xData[pos1];
                Toast.makeText(SentimentActivity.this,"Opinion: "+Opinion+"\n"+"Tweets: "+tweets+"%",Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onNothingSelected() {
                Log.i("PieChart", "nothing selected");
            }
        });

        /** Code for PieChart - End*/

        /** Code for Line Chart - Start*/
        lineChart=(LineChart)findViewById(R.id.lineChart);
        lineChart.getDescription().setText("Tweets' sentiments");
        yValues.add(new Entry(0,0f));

        lineChart.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

                // un-highlight values after the gesture is finished and no single-tap
                if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
                    // or highlightTouch(null) for callback to onNothingSelected(...)
                    lineChart.highlightValues(null);
            }

            @Override
            public void onChartLongPressed(MotionEvent me) {
                Log.i("LongPress", "Chart longpressed.");
            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {
                Log.i("DoubleTap", "Chart double-tapped.");
            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {
                Log.i("SingleTap", "Chart single-tapped.");
            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
                Log.i("Fling", "Chart flinged. VeloX: "
                        + velocityX + ", VeloY: " + velocityY);
            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
                Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {
                Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
            }
        });

        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.i("Entry selected", e.toString());
//                Log.i("LOWHIGH", "low: " + lineChart.getLowestVisibleXIndex()
//                        + ", high: " + lineChart.getHighestVisibleXIndex());

                Log.i("MIN MAX", "xmin: " + lineChart.getXChartMin()
                        + ", xmax: " + lineChart.getXChartMax()
                        + ", ymin: " + lineChart.getYChartMin()
                        + ", ymax: " + lineChart.getYChartMax());
            }

            @Override
            public void onNothingSelected() {
                Log.i("Nothing selected", "Nothing selected.");
            }
        });

        // enable touch gestures
        lineChart.setTouchEnabled(true);
        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        // remove labels from right side
        YAxis yAxisRight = lineChart.getAxisRight();
        yAxisRight.setEnabled(false);

        /** Code for LineChart - End*/


    }
    @Override
    protected void onResume(){
        //start handler as activity become visible

        h.postDelayed( runnable = new Runnable() {
            public void run() {
                getSentiment();

                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            // Handle the home action
            Intent intent=new Intent(SentimentActivity.this, DashboardActivity.class);
            startActivity(intent);
        } else if (id == R.id.real_time) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if(id == R.id.week_analysis){
            Intent intent = new Intent(this, WeekActivity.class);
            startActivity(intent);
        } else if(id == R.id.text_analyze){
            Intent intent = new Intent(this, TextAnalyzeActivity.class);
            startActivity(intent);
        } else if (id == R.id.about) {
            Intent intent=new Intent(SentimentActivity.this,AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.developer_team) {
            Intent intent=new Intent(SentimentActivity.this,DeveloperTeamActivity.class);
            startActivity(intent);

        } else if (id == R.id.help){
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id==R.id.nav_share)
        {
            Intent i=new Intent(android.content.Intent.ACTION_SEND);
            i.setType("text/plain");
            startActivity(Intent.createChooser(i,"Share Via"));
        } else {
            Log.d("Navigation Drawer","No Activity is selected");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void addDataSet() {

        Log.d(TAG,"addDataSet: Started");
        ArrayList<PieEntry> yEntrys = new ArrayList<PieEntry>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i=0;i< yData.length;i++)
        {
            yEntrys.add(new PieEntry(yData[i],i));
        }


        for(int i=0;i< xData.length;i++)
        {
            xEntrys.add(xData[i]);
        }

        //Completed_TODO create the data set
        PieDataSet pieDataSet= new PieDataSet(yEntrys,"Popularity");
        pieDataSet.setSliceSpace(5);
        pieDataSet.setValueTextSize(20);


        //Adding Colors to DataSet
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(104, 159, 56));  // holo green (Material green 700)
        colors.add(Color.rgb(211, 47, 47));
        colors.add(Color.rgb(97, 97, 97));

        pieDataSet.setColors(colors);


        //Adding a legend to chart

        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        List<LegendEntry> labels = new ArrayList<>();
        for (int i = 0; i < xEntrys.size(); i++) {
            LegendEntry entry = new LegendEntry();
            entry.formColor = colors.get(i);
            entry.label = xEntrys.get(i);
            labels.add(entry);
        }

        legend.setCustom(labels);

        //create pie data object
        PieData pieData = new PieData(pieDataSet);

        //to show data in percentage format
        pieData.setValueFormatter(new PercentFormatter());

        pieChart.setData(pieData);
        pieChart.invalidate();

    }

    private void addLineDataSet(long xValue, String Score) {

        float scoreValue=Float.parseFloat(Score);
        yValues.add(new Entry(xValue,scoreValue));

        LineDataSet set1= new LineDataSet(yValues, "Sentiment score");

        // fill the color below line
//        set1.setDrawFilled(true);
        set1.setColors(ColorTemplate.COLORFUL_COLORS);
        set1.setFillAlpha(110);
//        set1.setColor(Color.RED);  //Changing Color of Line
        set1.setLineWidth(3f);
        set1.setValueTextSize(12f);
        set1.setValueTextColor(Color.BLACK);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        LineData lineData = new LineData(dataSets);

        lineChart.setData(lineData);

    }


    /**
     * Call Sentiment API from django
     */
    @SuppressLint("StaticFieldLeak")
    public void getSentiment() {


        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog = new ProgressDialog(SentimentActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                String query= null;

                try {
                    Intent i = getIntent();
                    Bundle bundle = i.getExtras();

                    if(bundle != null){
                        if(bundle.getString("searchQuery")!=null)
                            query = bundle.getString("searchQuery");

                    }

                    URL url = new URL("https://tweetsenseapp.herokuapp.com/stream?query="+query.replaceAll("\\s+", "+"));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    String token = "Token " + accessToken;
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Authorization", token);
                    Log.d("sentiment0", "called");
                    Log.d("URL",url.toString());



                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    Log.d("sentiment1", "called");

                    StringBuffer json = new StringBuffer(1024);
                    String tmp = "";

                    while((tmp = reader.readLine()) != null)
                        json.append(tmp).append("\n");
                    reader.close();
                    Log.d("sentiment2", "called");

                    data = new JSONObject(json.toString());
                    Log.d("sentiment3", "called");

                    if(data.getInt("cod") != 200) {
                        System.out.println("Cancelled");
                        return null;
                    }


                } catch (Exception e) {

                    System.out.println("Exception "+ e.getMessage());
                    return null;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void Void) {
                if(data!=null){

                    Log.d("sentiment received",data.toString());
                    try{
                        float percent_pos,percent_neg,percent_neutral,positive,negative,Neutral;

                        String score = data.getString("score");
                        sentimentScoreField.setText(score);
                        pos = data.getString("pos");
                        neg = data.getString("neg");
                        neutral = data.getString("neutral");
                        Log.d("pos:", pos);
                        Log.d("neg:", neg);
                        Log.d("neutral:", neutral);

                        //assigning values to positive, negative and Neutral
                        positive=Float.parseFloat(pos);
                        negative=Float.parseFloat(neg);
                        Neutral=Float.parseFloat(neutral);

                        //Calculating Percentage
                        percent_pos= (positive*100)/(positive+negative+Neutral);
                        percent_neg= (negative*100)/(positive+negative+Neutral);
                        percent_neutral= (Neutral*100)/(positive+negative+Neutral);

                        // Formatting data upto 2 decimal points
                        DecimalFormat f = new DecimalFormat("###.##");
                        yData[0]= (float) Double.parseDouble(f.format(percent_pos));
                        yData[1]= (float) Double.parseDouble(f.format(percent_neg));
                        yData[2]= (float) Double.parseDouble(f.format(percent_neutral));

                        //here we get the result for the pie chart so making the progress bar as invisible
                        mProgressBar.setVisibility(View.GONE);
                        sentimentScoreTitle.setVisibility(View.VISIBLE);
                        sentimentScoreField.setVisibility(View.VISIBLE);
                        pieChart.setVisibility(View.VISIBLE);
                        lineChart.setVisibility(View.VISIBLE);


                        timeValue=timeValue+10;
                        addLineDataSet(timeValue,score);
                        addDataSet();

                    }
                    catch (Exception e){
                        Log.e("Sentiment", "One or more fields not found in the JSON data");
                    }

                }

                dialog.dismiss();
            }

        }.execute();

    }




    /* stop activity */
    public void onClickStop(View v){
        Toast.makeText(this, "Analysis stopped!", Toast.LENGTH_SHORT).show();
//        Intent intent = getIntent();
//        finish();
//        startActivity(intent);
        h.removeCallbacks(runnable);
    }


}
