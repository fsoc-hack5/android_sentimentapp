package com.herokuapp.tweetsenseapp.sentimentapp;

/**
 * Created by harshit on 14/7/18.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            /*  open welcome activity (opens for first time users only) */
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();

        } catch (Exception e){
            Log.d("activity crash", "Can't open MainActivity!");
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
//            finish();
        }

    }
}
