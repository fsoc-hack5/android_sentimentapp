package com.herokuapp.tweetsenseapp.sentimentapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class HelpActivity extends AppCompatActivity {

    public static boolean startFromHelp = false;
    EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        // For back button
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        message = findViewById(R.id.message);
    }

    /**
     * Navigate Up to Parent Activity
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openTutorial(View view){
//        set startFromHelp to true to see tutorial
        startFromHelp = true;
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finish();

    }

    public void sendEmail(View v){
        // submit issue by email
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("message/rfc822");  // for email client
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"harshitk.997@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Issue regarding SentimentApp");
        i.putExtra(Intent.EXTRA_TEXT, message.getText().toString());
        try {
            startActivity(Intent.createChooser(i, "Send email..."));
        } catch (android.content.ActivityNotFoundException ex){
            Toast.makeText(this, "There are no email clients installed!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openFAQ(View view) {
        Uri uri = Uri.parse("https://rajasthansentiment.herokuapp.com/faq");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);
    }
}
