package com.herokuapp.tweetsenseapp.sentimentapp;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

public class TextAnalyzeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Button analyzeTextBtn;
    EditText etAnalyzeText;
    ProgressBar mProgressBar;
    TextView tv_sentiment_score;
    TextView tv_sentiment;
    JSONObject data = null;
    LinearLayout sentimentShow;
    TextView tvTone;
    private String accessToken = "3ca63768cbdca6fd6e3ffbc5bfe3a6eca94b2ed1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_analyze);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        sentimentShow=(LinearLayout)findViewById(R.id.sentimentShow);
        etAnalyzeText = (EditText) findViewById(R.id.etAnalyzeText);
        analyzeTextBtn = (Button) findViewById(R.id.analyzeTextBtn);
        mProgressBar = (ProgressBar) findViewById(R.id.mProgressBar);
        tv_sentiment = (TextView) findViewById(R.id.tv_sentiment);
        tv_sentiment_score = (TextView) findViewById(R.id.tv_sentiment_score);
        tvTone=(TextView)findViewById(R.id.tvTone);

        analyzeTextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText=etAnalyzeText.getText().toString();
                if (searchText.isEmpty()) {
                    etAnalyzeText.setError(getString(R.string.input_error_text));
                    etAnalyzeText.requestFocus();
//                    return;
                }
                else {
                    sentimentShow.setVisibility(View.GONE);
                    Toast.makeText(TextAnalyzeActivity.this,"Analyzing Text",Toast.LENGTH_SHORT).show();
                    getSentiment();

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        } else if (id == R.id.real_time) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if(id == R.id.week_analysis){
            Intent intent = new Intent(this, WeekActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }  else if (id == R.id.about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.developer_team) {
            Intent intent = new Intent(this, DeveloperTeamActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.help) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.nav_share) {
            Intent i = new Intent(android.content.Intent.ACTION_SEND);
            i.setType("text/plain");
            startActivity(Intent.createChooser(i, "Share Via"));
        } else {
            Log.d("Navigation Drawer", "No Activity is selected");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Call Sentiment API from django
     */
    @SuppressLint("StaticFieldLeak")
    public void getSentiment() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... params) {
                String query = etAnalyzeText.getText().toString();
                Log.d("Query", query);

                try {
                    URL url = new URL("https://tweetsenseapp.herokuapp.com/analyze?query=" + query.replaceAll("\\s+", "+"));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    String token = "Token " + accessToken;
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Authorization", token);
                    Log.d("URL", url.toString());


                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    Log.d("sentiment1", "called");

                    StringBuffer json = new StringBuffer(1024);
                    String tmp = "";

                    while ((tmp = reader.readLine()) != null)
                        json.append(tmp).append("\n");
                    reader.close();
                    Log.d("sentiment2", "called");

                    data = new JSONObject(json.toString());
                    Log.d("sentiment3","called");


                    if (data.getInt("cod") != 200) {
                        System.out.println("Cancelled");
                        return null;
                    }

                } catch (Exception e) {

                    System.out.println("Exception " + e.getMessage());
                    return null;
                }

                return null;
            }


            @SuppressLint("ResourceAsColor")
            @Override
            protected void onPostExecute(Void Void) {
                if (data != null) {
                    Log.d("sentiment received", data.toString());
                    try {
                        String sentiment , score;
                        score = data.getString("score");
                        sentiment = data.getString("polarity");

                        JSONObject tone_names = data.getJSONObject("tone");

                        String tone="";

                        if (!tone_names.isNull("Joy"))
                            tone=tone+"Joy: "+String.valueOf(tone_names.getDouble("Joy")) + "\n";

                        if (!tone_names.isNull("Analytical")){
                            tone=tone+"Analytical: "+String.valueOf(tone_names.getDouble("Analytical"))+"\n";
                        }

                        if (!tone_names.isNull("Anger")){
                            tone=tone+"Anger: "+String.valueOf(tone_names.getDouble("Anger"))+"\n";
                        }

                        if (!tone_names.isNull("Fear")){
                            tone=tone+"Fear: "+String.valueOf(tone_names.getDouble("Fear"))+"\n";
                        }

                        if (!tone_names.isNull("Sadness")){
                            tone=tone+"Sadness: "+String.valueOf(tone_names.getDouble("Sadness"))+"\n";
                        }

                        if (!tone_names.isNull("Confident")){
                            tone=tone+"Confident: "+String.valueOf(tone_names.getDouble("Confident"))+"\n";
                        }

                        if (!tone_names.isNull("Tentative")){
                            tone=tone+"Tentative: "+String.valueOf(tone_names.getDouble("Tentative"))+"\n";
                        }

                        Log.d("tone",tone);

                        tvTone.setText(tone);


                        //set color based on sentiment score
                        switch (sentiment) {
                            case "Highly Negative":
                                tv_sentiment_score.setTextColor(getResources().getColor(R.color.holo_red_dark, null));
                                tv_sentiment.setTextColor(getResources().getColor(R.color.holo_red_dark, null));
                                tvTone.setTextColor(getResources().getColor(R.color.holo_red_dark, null));

                                break;
                            case "Negative":
                                tv_sentiment_score.setTextColor(getResources().getColor(R.color.holo_red, null));
                                tv_sentiment.setTextColor(getResources().getColor(R.color.holo_red, null));
                                tvTone.setTextColor(getResources().getColor(R.color.holo_red, null));

                                break;
                            case "Positive":
                                tv_sentiment_score.setTextColor(getResources().getColor(R.color.holo_green, null));
                                tv_sentiment.setTextColor(getResources().getColor(R.color.holo_green, null));
                                tvTone.setTextColor(getResources().getColor(R.color.holo_green, null));

                                break;
                            case "Highly Positive":
                                tv_sentiment_score.setTextColor(getResources().getColor(R.color.holo_green_dark, null));
                                tv_sentiment.setTextColor(getResources().getColor(R.color.holo_green_dark, null));
                                tvTone.setTextColor(getResources().getColor(R.color.holo_green_dark, null));

                                break;
                            default:
                                tv_sentiment_score.setTextColor(getResources().getColor(R.color.holo_orange_dark, null));
                                tv_sentiment.setTextColor(getResources().getColor(R.color.holo_orange_dark, null));
                                tvTone.setTextColor(getResources().getColor(R.color.holo_orange_dark, null));

                                break;
                        }
                        sentimentShow.setVisibility(View.VISIBLE);
                        tv_sentiment.setText(sentiment);
                        tv_sentiment_score.setText(score);



                    } catch (Exception e) {
                        Log.e("Sentiment", "One or more fields not found in the JSON data");
                    }

                }

                mProgressBar.setVisibility(View.GONE);
            }

        }.execute();

    }

}
