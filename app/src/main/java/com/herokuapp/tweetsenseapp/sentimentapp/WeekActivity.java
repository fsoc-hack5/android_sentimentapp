package com.herokuapp.tweetsenseapp.sentimentapp;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class WeekActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    JSONObject data = null;
    private String pos, neg, neutral;

    private static String TAG = "WeekActivity";

    private float[] yData = new float[3];

    private String[] xData = {"Positive","Negative","Neutral"};

    PieChart pieChart;

    TextView tv_sentimentScore;
    TextView sentimentScoreField;
    ProgressBar mProgressBar;
    Button btn_get_sentiment_score;
    EditText etSearchQuery;
    private String accessToken = "3ca63768cbdca6fd6e3ffbc5bfe3a6eca94b2ed1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Log.d(TAG, "created!");

        //assigning and staring the progress bar
        mProgressBar=(ProgressBar)findViewById(R.id.mProgressBar);
        btn_get_sentiment_score=(Button)findViewById(R.id.btn_get_sentiment_score);
        etSearchQuery=(EditText)findViewById(R.id.etSearchQuery);
//        mProgressBar.setVisibility(View.VISIBLE);

        //Assinging id
        tv_sentimentScore = findViewById(R.id.tv_sentimentScore);
        sentimentScoreField = (TextView)findViewById(R.id.txtSentimentScore);

        btn_get_sentiment_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchQuery=etSearchQuery.getText().toString();
                if (searchQuery.isEmpty()) {
                    etSearchQuery.setError(getString(R.string.input_error_query));
                    etSearchQuery.requestFocus();
//                    return;
                }
                else {
                    //setting visibility to gone onClick
                    pieChart.setVisibility(View.GONE);

                    getSentiment();
                }

            }
        });


        Log.d(TAG,"onCreate: starting to create chart");

        pieChart=(PieChart)findViewById(R.id.pieChart);

        pieChart.setContentDescription("Tweets' sentiments");
        pieChart.getDescription().setText("Tweets' sentiments");
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Sentiment Chart");
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);

        //To animate piechart as per the time selected in spinner
//        Intent i=getIntent();
//        String pieChartTime=i.getStringExtra("time");
        String pieChartTime = "2";  // estimated time of reply from server
        int time=Integer.parseInt(pieChartTime); //here time is in seconds
        Log.d("Spinner Time",pieChartTime);
        //converting time to miliseconds
//        pieChart.animateXY((time*1000+5000),(time*1000+5000));



        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.d(TAG, "onValueSelected: Value select from chart");
                Log.d(TAG,"onValueSelected "+ e.toString());
                Log.d(TAG,"onValueSelected "+ h.toString());

                int pos1 = e.toString().indexOf("y: "); // total characters in "y: " = 3(Three)
                String tweets= e.toString().substring(pos1 + 3);

                Log.d(TAG,"Sentiment Score: "+tweets);

                for (int i=0;i<yData.length;i++){
                    if(yData[i]==Float.parseFloat(tweets)){
                        pos1=i;
                        break;
                    }
                }
                String Opinion =xData[pos1];
                Toast.makeText(WeekActivity.this,"Opinion: "+Opinion+"\n"+"Tweets: "+tweets+"%",Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onNothingSelected() {
                Log.i("PieChart", "nothing selected");
            }
        });

    }


    //No need of OnResume and On Pause Method as it calls Rest Api

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        } else if (id == R.id.real_time) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if(id == R.id.text_analyze){
            Intent intent = new Intent(this, TextAnalyzeActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.developer_team) {
            Intent intent = new Intent(this, DeveloperTeamActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.help) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.nav_share) {
            Intent i = new Intent(android.content.Intent.ACTION_SEND);
            i.setType("text/plain");
            startActivity(Intent.createChooser(i, "Share Via"));
        } else {
            Log.d("Navigation Drawer", "No Activity is selected");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void addDataSet() {

        Log.d(TAG,"addDataSet: Started");
        ArrayList<PieEntry> yEntrys = new ArrayList<PieEntry>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i=0;i< yData.length;i++)
        {
            yEntrys.add(new PieEntry(yData[i],i));
        }


        for(int i=0;i< xData.length;i++)
        {
            xEntrys.add(xData[i]);
        }

        //Completed_TODO create the data set
        PieDataSet pieDataSet= new PieDataSet(yEntrys,"Popularity");
        pieDataSet.setSliceSpace(5);
        pieDataSet.setValueTextSize(20);


        //Adding Colors to DataSet
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(104, 159, 56));  // holo green (Material green 700)
        colors.add(Color.rgb(211, 47, 47));
        colors.add(Color.rgb(97, 97, 97));

        pieDataSet.setColors(colors);


        //Adding a legend to chart

        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        List<LegendEntry> labels = new ArrayList<>();
        for (int i = 0; i < xEntrys.size(); i++) {
            LegendEntry entry = new LegendEntry();
            entry.formColor = colors.get(i);
            entry.label = xEntrys.get(i);
            labels.add(entry);
        }

        legend.setCustom(labels);

        //create pie data object
        PieData pieData = new PieData(pieDataSet);

        //to show data in percentage format
        pieData.setValueFormatter(new PercentFormatter());

        pieChart.setData(pieData);
        pieChart.invalidate();

    }




    /**
     * Call Sentiment API from django
     */

    @SuppressLint("StaticFieldLeak")
    public void getSentiment() {


        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog = new ProgressDialog(WeekActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... params) {
                String query= null;

                try {
                        if(etSearchQuery.getText().toString()!=null)
                            query = etSearchQuery.getText().toString();
                        else
                        {
                            Toast.makeText(WeekActivity.this,"Please Write Something in Search Query",Toast.LENGTH_SHORT).show();
                        }

                    URL url = new URL("https://tweetsenseapp.herokuapp.com/rest?query="+query.replaceAll("\\s+", "+"));
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    String token = "Token " + accessToken;
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Authorization", token);
                    Log.d("URL",url.toString());



                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    Log.d("sentiment1", "called");

                    StringBuffer json = new StringBuffer(1024);
                    String tmp = "";

                    while((tmp = reader.readLine()) != null)
                        json.append(tmp).append("\n");
                    reader.close();
                    Log.d("sentiment2", "called");

                    data = new JSONObject(json.toString());
                    Log.d("sentiment3", "called");

                    if(data.getInt("cod") != 200) {
                        System.out.println("Cancelled");
                        return null;
                    }


                } catch (Exception e) {

                    System.out.println("Exception "+ e.getMessage());
                    return null;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void Void) {
                if(data!=null){

                    mProgressBar.setVisibility(View.GONE);
                    pieChart.setVisibility(View.VISIBLE);
                    tv_sentimentScore.setVisibility(View.VISIBLE);
                    Log.d("sentiment received",data.toString());
                    try{
                        float percent_pos,percent_neg,percent_neutral,positive,negative,Neutral;

                        sentimentScoreField.setText(data.getString("score"));
                        pos = data.getString("pos");
                        neg = data.getString("neg");
                        neutral = data.getString("neutral");
                        Log.d("pos:", pos);
                        Log.d("neg:", neg);
                        Log.d("neutral:", neutral);

                        //assigning values to positive, negative and Neutral
                        positive=Float.parseFloat(pos);
                        negative=Float.parseFloat(neg);
                        Neutral=Float.parseFloat(neutral);

                        //Calculating Percentage
                        percent_pos= (positive*100)/(positive+negative+Neutral);
                        percent_neg= (negative*100)/(positive+negative+Neutral);
                        percent_neutral= (Neutral*100)/(positive+negative+Neutral);

                        // Formatting data upto 2 decimal points
                        DecimalFormat f = new DecimalFormat("###.##");
                        yData[0]= (float) Double.parseDouble(f.format(percent_pos));
                        yData[1]= (float) Double.parseDouble(f.format(percent_neg));
                        yData[2]= (float) Double.parseDouble(f.format(percent_neutral));

                        //here we get the result for the pie chart so making the progress bar as invisible
                        mProgressBar.setVisibility(View.INVISIBLE);

                        addDataSet();

                    }
                    catch (Exception e){
                        Log.e("Sentiment", "One or more fields not found in the JSON data");
                    }

                }

                dialog.dismiss();
            }

        }.execute();

    }

}
