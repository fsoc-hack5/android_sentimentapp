package com.herokuapp.tweetsenseapp.sentimentapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }


    public void openMainActivity(View v){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void openTextAnalyzeActivity(View v){
        Intent i = new Intent(this, TextAnalyzeActivity.class);
        startActivity(i);
    }

    public void openWeekActivity(View v){
        Intent i = new Intent(this, WeekActivity.class);
        startActivity(i);
    }
}
